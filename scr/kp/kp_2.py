from src.kp import kp_1
import matplotlib.ticker as tcr
import matplotlib.pyplot as plt

a, x1, x2 = 13, 0, 100  # параметры для функции
real_val = 842321542  # 842321541.666667
qnt_step_start, qnt_step_stop, step = 4, 101, 4  # кол-во шагов при интегрировании

# Заполнение массива с реальным значением интегрирования функции
mass_real = [real_val for i in range(qnt_step_start, qnt_step_stop, step)]

# Заполнение массивов с подсчитанными данными интегрирования функции
mass_val_rectangle = [kp_1.rectangle(a, x1, x2, i) for i in range(qnt_step_start, qnt_step_stop, step)]
mass_val_trapezoid = [kp_1.trapezoid(a, x1, x2, i) for i in range(qnt_step_start, qnt_step_stop, step)]
mass_val_simpson = [kp_1.simpson(a, x1, x2, i) for i in range(qnt_step_start, qnt_step_stop, step)]

# Заполнение массива с кол-вом шагов при интегрировании
mass_qnt_step = [i for i in range(qnt_step_start, qnt_step_stop, step)]

size_column = step/6

mass_qnt_in_bar_1 = [mass_qnt_step[_] - (size_column + size_column / 2) for _ in range(len(mass_qnt_step))]
mass_qnt_in_bar_2 = [mass_qnt_step[_] - size_column / 2 for _ in range(len(mass_qnt_step))]
mass_qnt_in_bar_3 = [mass_qnt_step[_] + size_column / 2 for _ in range(len(mass_qnt_step))]
mass_qnt_in_bar_4 = [mass_qnt_step[_] + (size_column + size_column / 2) for _ in range(len(mass_qnt_step))]

# Построение графика
ax = plt.subplot()
plt.bar(mass_qnt_in_bar_1, mass_real, label='Реальное значение')
plt.bar(mass_qnt_in_bar_2, mass_val_rectangle, label='Метод прямоугольников')
plt.bar(mass_qnt_in_bar_3, mass_val_trapezoid, label='Метод трапеций')
plt.bar(mass_qnt_in_bar_4, mass_val_simpson, label='Метод Симпсона')
# plt.plot(mass_qnt_step, mass_real, label='Реальное значение')
# plt.plot(mass_qnt_step, mass_val_rectangle, marker='o', ms=3, label='Метод прямоугольников')
# plt.plot(mass_qnt_step, mass_val_trapezoid, marker='o', ms=3, label='Метод трапеций')
# plt.plot(mass_qnt_step, mass_val_simpson, marker='o', ms=3, label='Метод Симпсона')
plt.title('Точность интегрирования относительно реального значения')
plt.xlabel('Кол-во шагов при интегрировании')
plt.ylabel('Значение интегрирования')
plt.legend(fontsize=8, ncol=2, facecolor='white', edgecolor='black', title='Сортировки', title_fontsize='8')
ax.xaxis.set_major_locator(tcr.MultipleLocator(step))
plt.grid()
plt.show()


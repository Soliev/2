from typing import List

import lab_2
import random
import matplotlib.pyplot as plt
import matplotlib.ticker as tcr
import cProfile
import pstats
from pstats import SortKey

# Данные для цикла заполнения массивов данных
start, stop, step = 1000, 10000, 1000

# Заполнение массива с массивами рандомных чисел
completion_num = [[random.randint(0, 50) for _ in range(i)] for i in range(start, stop + 1, step)]

# Заполнение массива рандомными числами из mass_in
mass_var_num = [completion_num[_][random.randint(0, len(completion_num[_]))] for _ in range(len(completion_num))]

mass_in_str, mass_var_str = [], []

# Заполнение массива рандомных строк и массива частей строк, которые необходимо найти
for i in range(start, stop + 1, step):
    str_in = ''
    str_var = ''
    for _ in range(int(i / 2)):
        str_in += random.SystemRandom().choice(['ab', 'ac', 'ad', 'bc', 'cd', 'bd'])
    for _ in range(6):
        str_var += random.SystemRandom().choice(['ab', 'ac', 'ad', 'bc', 'cd', 'bd'])
    mass_in_str.append(str_in)
    mass_var_str.append(str_var)

# Заносим кол-во элементов участвующих в сортировке для построения графика
mass_number = [len(completion_num[_]) for _ in range(len(completion_num))]

mass_qnt_iterations_1, mass_qnt_iterations_2, mass_qnt_iterations_3, mass_qnt_iterations_4 = [], [], [], []

i = 0
print(' Идет подсчет кол-ва операций поиска в структуре данных. Подождите немного.')
for sign in completion_num:
    print('Кол-во данных:', len(sign))
    var = 0
    while True:
        if var == 0:
            cProfile.run('lab_2.linear(sign, mass_var_num[i])', 'stats.log')
        elif var == 1:
            mass_qnt_iterations_1.append(int(num_of_oper[0].split()[0]))
            cProfile.run('lab_2.binary(sorted(sign), mass_var_num[i])', 'stats.log')

        with open('output.txt', 'w') as log_file_stream:
            p = pstats.Stats('stats.log', stream=log_file_stream)
            p.strip_dirs().sort_stats(SortKey.NAME == 'swap').print_stats()

        with open('output.txt') as f:
            lines = [line.strip() for line in f]

        num_of_oper: list[str] = [line for line in lines if line not in '' and line.count('swap')]

        var += 1

        if var == 2:
            mass_qnt_iterations_2.append(int(num_of_oper[0].split()[0]))
            break
    i += 1

i = 0
print('Идет вычисление операций. Это может занять какое-то время.')
for sign in mass_in_str:
    print('Кол-во данных:', len(sign))
    var = 0
    while True:
        if var == 0:
            cProfile.run('lab_2.naive(sign, mass_var_str[i])', 'stats.log')
        elif var == 1:
            mass_qnt_iterations_3.append(int(num_of_oper[0].split()[0]))
            cProfile.run('lab_2.KMP(sign, mass_var_str[i])', 'stats.log')

        with open('output.txt', 'w') as log_file_stream:
            p = pstats.Stats('stats.log', stream=log_file_stream)
            p.strip_dirs().sort_stats(SortKey.NAME == 'swap').print_stats()

        with open('output.txt') as f:
            lines = [line.strip() for line in f]

        num_of_oper = [line for line in lines if line not in '' and line.count('swap')]

        var += 1

        if var == 2:
            mass_qnt_iterations_4.append(int(num_of_oper[0].split()[0]))
            break
    i += 1

print('Построение графика.')

# График кол-ва операций
ax = plt.subplot()
plt.plot(mass_number, mass_qnt_iterations_1, marker='o', ms=3, label='Линейный')
plt.plot(mass_number, mass_qnt_iterations_2, marker='o', ms=3, label='Бинарный')
plt.plot(mass_number, mass_qnt_iterations_3, marker='o', ms=3, label='Наивный')
plt.plot(mass_number, mass_qnt_iterations_4, marker='o', ms=3, label='Кнута-Морриса-Пратта')
plt.title('Кол-ва операций')
plt.xlabel('Кол-во чисел')
plt.ylabel('Кол-во операций')
plt.legend(fontsize=8, ncol=2, facecolor='white', edgecolor='black', title='Поиск', title_fontsize='8')
ax.set_xlim(mass_number[0], mass_number[-1])
ax.xaxis.set_major_locator(tcr.MultipleLocator(step * 2))
plt.grid()
plt.show()

